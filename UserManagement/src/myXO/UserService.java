package myXO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserService {

    private static ArrayList<User> list = new ArrayList();

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            list = (ArrayList<User>)ois.readObject();
        } catch (FileNotFoundException e) {
            Logger.getLogger(testReadFile.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(testReadFile.class.getName()).log(Level.SEVERE, null, e);
        } catch (ClassNotFoundException e) {
            Logger.getLogger(testReadFile.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    public static void save(ArrayList<User> list) {
        UserService.list = list;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            File file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException e) {
            Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, e);
            }
        }

    }

    public static boolean auth(String loginName, String password) {
        for (User user : list) {
            if (user.getLoginName().equals(loginName) && user.getPassword().equals(password)) {
                return true;
            }

        }
        return false;

    }
}
